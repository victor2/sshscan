#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include "Poco/Runnable.h"
#include "Poco/Thread.h"
#include "Poco/Net/NetworkInterface.h"
#include "Poco/Net/DNS.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/Net/SocketStream.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/StreamSocketImpl.h"
#include "Poco/String.h"

using Poco::Net::IPAddress;
using Poco::Net::SocketAddress;
using Poco::Net::StreamSocket;
using Poco::Net::StreamSocketImpl;
using Poco::Net::NetworkInterface;
using Poco::Net::HostEntry;
using Poco::Net::DNS;

using namespace std;

vector<string> splitString(string source, char sep)
{
	vector<string> result;
	std::size_t pos = 0;
	while (true) {
        std::size_t idx = source.find(sep, pos);
        string sub = (idx != string::npos) ? source.substr(pos, idx - pos) : source.substr(pos);
		if (!sub.empty()) {
			result.push_back(sub);
		}
		if (idx == string::npos) {
			break;
		}
		pos = idx + 1;
	}
	return result;
}

template <class T>
string toString(const T& value){
	ostringstream out;
	out << value;
	return out.str();
}


class SshTester: public Poco::Runnable {

    string thisAddress;
public:
    SshTester(IPAddress &ip, int idx) {
        vector<string> subs = splitString(ip.toString(), '.');
        thisAddress = subs[0] + "." + subs[1] + "." + subs[2] + "." + toString(idx);
    }

    void run() {
        try {
            // cout << "(" << thisAddress << ")";
            SocketAddress address(thisAddress, 22);
            StreamSocket s(address);
            Poco::Net::SocketStream str(s);
            str << "hello";
            str.flush();
            cout << "Found " << thisAddress << endl;
            s.close();
        } catch (exception e) {
        }
    }
};

//========================================================================
int main( ) {

    Poco::Net::initializeNetwork();

    vector<string> ips;
    NetworkInterface::NetworkInterfaceList ifcs = NetworkInterface::list();
    for (int i=0; i<ifcs.size(); )
    {
        NetworkInterface ni = ifcs[i];
        IPAddress ip = ni.address();
        cout << (++i) << ". " << ip.toString() << " " << ni.displayName() << endl;
        ips.push_back(ip.toString());
    }

    int ifNum;
    do {
        cout << "Select the interface: ";
        cin >> ifNum;
    } while (ifNum < 1 || ifNum > ifcs.size());

    IPAddress ip = ifcs[ifNum - 1].address();

    cout << "Scanning on " << ip.toString() << endl;
    for (int i = 0; i < 255; i++) {
        SshTester *tester = new SshTester(ip, i);
        Poco::Thread *thr = new Poco::Thread();
        thr->start(*tester);
    }

    Poco::Thread::sleep(1000);
    Poco::Net::uninitializeNetwork();

    cout << "Press Ctrl-C to close";
    cin >> ifNum;
}
